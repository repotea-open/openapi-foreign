curl -X POST ${HOST}/api/v1/product/modify \
  --header "Authorization: Bearer ${TOKEN}" \
  --header 'Content-Type: application/json' \
  --data-raw '{
      "id": "365880768691572736",
      "name": "Album 011",
      "vendor_id": "365764804490694656",
      "code": "a01",
      "description": "This is a good product"
  }'
