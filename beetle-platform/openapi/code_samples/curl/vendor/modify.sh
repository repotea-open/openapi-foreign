curl -X POST ${HOST}/api/v1/vendor/modify \
  --header "Authorization: Bearer ${TOKEN}" \
  --header 'Content-Type: application/json' \
  --data-raw '{
    "id": "365764804490694660",
    "name": "Blunk LLC",
    "logo": "https://www.gravatar.com/avatar?s=128&d=identicon&r=PG",
    "description": "We are the best vendor",
    "contact": "Alice",
    "phone": "+884 1203985",
    "mobile": "+1 139 586 222",
    "address": "(520) 648-1053\n1698 W Baltusrol Dr\nGreen Valley, Arizona(AZ), 85614\n"
  }'
